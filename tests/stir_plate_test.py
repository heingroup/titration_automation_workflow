from typing import List

from ftdi_serial import Serial
from ika import MagneticStirrer
from new_era.peristaltic_pump import PeristalticPump

ports: List[str] = Serial.list_device_ports()
def stir_plate_port():


    stir_plate = ''
    for port in ports:
        try:
            MagneticStirrer(device_port=port)
            stir_plate = port
        except:
            print("Testing anthoer port...")
    return stir_plate


port = stir_plate_port()
if port == '':
    print('Test could not find the port for stir plate. Please make sure no other scripts are running'
          ' and try again later. If this problem is still occurring, try replug in the stir plate and test again')
else:
    print('The port for stir plate is ' + port + '. Please copy that and paste into /titration/main.py line XX.')

def peri_pump_ports():
    peri_pumps = []
    for port in ports:
        try:
            PeristalticPump(port=port)
            peri_pumps.append(port)
        except:
            print('testing pumps')
    return peri_pumps

