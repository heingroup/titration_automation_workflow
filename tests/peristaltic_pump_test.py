import time
from typing import List

from ftdi_serial import Serial

from new_era.peristaltic_pump import PeristalticPump


ports: List[str] = Serial.list_device_ports()

peri_pumps = []
number = 1
for port in ports:
    try:
        pump = PeristalticPump(port=port)
        if number == 1:
            print('The dispense pump\'s port is: ' +port+'. ')
            time.sleep(3)
            print('The dispense pump will run for 10 seconds.'
              ' Please label this running pump as the dispense pump and note the direction.')
            number += 1
        elif number == 2:
            print('The dispense pump\'s port is: ' + port)
            time.sleep(3)
            print('The evacuate pump will run for 10 seconds.'
                  ' Please label this running pump as the evacuate pump and note the direction.')
            number +=1
        else:
            break
        time.sleep(3)
        pump.pump(pump_time=10,
                           direction="dispense",
                           rate=10
                           )
        peri_pumps.append(port)
    except:
        print('testing another pump...')

# port = peri_pumps
# if port == []:
#     print('Test could not find the port for stir plate. Please make sure no other scripts are running'
#           ' and try again later. If this problem is still occurring, try replug in the stir plate and test again')
# elif not len(port) == 2:
#     print('Abnormal number of pumps has been found. Please try again later. ')
#
# else:
#     print('The port for stir plate is ' + port[0]
#           + ' and ' + port[1] + '. ')
#     time.sleep(5)
#     print('The dispense pump will run for 10 seconds.'
#           ' Please label this running pump as the dispense pump and note the direction')
#     dispense_pump = PeristalticPump(port=port[0])
#     dispense_pump.pump(pump_time=10,
#                        direction="dispense",
#                        rate=10
#                        )
#     print('The evacuate pump will run for 10 seconds.'
#           ' Please label this running pump as the devacuate pump and note the direction')
#     time.sleep(10)
#     evacuate_pump = PeristalticPump(port=port[1])
#     evacuate_pump.pump(pump_time=10,
#                        direction="dispense",
#                        rate=10
#                        )
print('Please copy and paste the dispense pump\'s port: ' +
          peri_pumps[0] + ' and the evacuate pump\'s port: ' + peri_pumps[1] + ' to /titration/main line XX and XX.')
