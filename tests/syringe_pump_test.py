
from ftdi_serial import Serial
from north_devices.pumps.tecan_cavro import TecanCavro

serials = Serial.list_device_serials()
baud_rate = 9600  # Change this if test does not find pumps multiple times
for serial in serials:
    try:
        tecan_cavro_serial = Serial(device_serial=serial, baudrate=baud_rate)
        syringe_pump_1 = TecanCavro(tecan_cavro_serial, address=0, syringe_volume_ml=5)  # todo
        syringe_pump_1.home()
        print('The anaylte syringe pump is found. It will be run for testing. Please label'
              ' this pump as the analyte pump.')
        syringe_pump_1.dispense_ml(5, 2, 2, 10)
        syringe_pump_2 = TecanCavro(tecan_cavro_serial, address=1, syringe_volume_ml=5)  # todo
        syringe_pump_2.home()
        print('The titrant syringe pump is found. It will be run for testing. Please label'
              ' this pump as the titrant pump.')
        syringe_pump_2.dispense_ml(5, 2, 2, 10)
        print('The serial of the syringe pumps is ' + serial +'. Please copy and paste this into '
                                                              '/titration/main line XX.')
        break
    except:
        print('testing another serials...')

print('It seems that no serial device has been found.'
      ' Please try this again later or check the baud rate of the serial device in Device Manager/Ports')
