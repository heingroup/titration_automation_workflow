import os
from typing import Dict
import logging
import json
from ftdi_serial import Serial
from hein_utilities.files import Watcher
from north_devices.pumps.tecan_cavro import TecanCavro
from ika.magnetic_stirrer import MagneticStirrer
from new_era.peristaltic_pump import PeristalticPump
import time
from pathlib import Path

# todo: Instrument initialization. Make sure to change ports according to your system
# Stir plate info

port = 'COM14'
stir_plate = MagneticStirrer(device_port=port)

# Peristaltic pump info

pump_port_evacuate = 'COM10'  # check on your own system
peristaltic_pump_evacuate = PeristalticPump(port=pump_port_evacuate)

pump_port_dispense = 'COM13'  # check on your own system
peristaltic_pump_dispense = PeristalticPump(port=pump_port_dispense)

# Syringe pump info

tecan_cavro_serial_id = 'FT2YOS81'  # check on your own system
syringe_volume = 5.0  # mL, todo
tecan_cavro_serial = Serial(device_serial=tecan_cavro_serial_id, baudrate=9600)
syringe_pump_1 = TecanCavro(tecan_cavro_serial, address=0, syringe_volume_ml=syringe_volume)
syringe_pump_1.home()
syringe_pump_2 = TecanCavro(tecan_cavro_serial, address=1, syringe_volume_ml=syringe_volume)
syringe_pump_2.home()
pump_default_velocity = 4  # mL/min #changeable.

pump_waste_position = 1
pump_stock_position = 2
pump_reactor_position = 3

# todo: Make sure the path file is correct for the current folder. Check the Disc and Dropbox

path = r'D:\Dropbox\Voice (1)\Titration Test User\current'
control_folder = os.path.join(path, 'control')
speech_folder = os.path.join(path, 'speech')
activation_folder = os.path.join(path, 'activation')
data_folder = os.path.join(path, 'data')

# data files

titration_status = os.path.join(speech_folder, 'titration_status.txt')
final_report = os.path.join(speech_folder, 'final_report.txt')
current_action = os.path.join(speech_folder, 'current_action.txt')
stock_titrant = os.path.join(data_folder, 'stock_titrant.txt')
stock_analyte = os.path.join(data_folder, 'stock_analyte.txt')
max_reactor_volume = os.path.join(data_folder, 'max_reactor_volume.txt')
current_reactor_volume = os.path.join(data_folder, 'current_reactor_volume.txt')
activation = os.path.join(activation_folder, 'activation.txt')
exp_num = os.path.join(path, 'data/exp_num.txt')

watch = Watcher(
    path=control_folder,
    watchfor='.txt',
)
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


# Titration data. Used to keep the addition information

class TitrationData():
    total_volume_dispensed_analyte = 0
    total_volume_dispensed_titrant = 0
    total_solution_in_flask = 0
    remaining_titrant_stock = 0
    remaining_analyte_stock = 0
    max_reactor_volume = 0
    exp_num = 1


new_titration = TitrationData


# the stir plate starts stirring
def start_stirring():
    stir_plate.start_stirring()
    stir_plate.target_stir_rate = 250


# the stir plate stops stirring
def stop_stirring():
    stir_plate.stop_stirring()


# change the stir rate
def change_stir_rate(command):
    max_stir_rate = 1000
    if command == 'decrease':
        if stir_plate.target_stir_rate <= 75:
            stir_plate.target_stir_rate = stir_plate.target_stir_rate
        else:
            stir_plate.target_stir_rate = stir_plate.target_stir_rate / 2
    if command == 'increase':
        if stir_plate.target_stir_rate >= max_stir_rate / 2:
            stir_plate.target_stir_rate = (stir_plate.target_stir_rate
                                           + max_stir_rate) / 2
        else:
            stir_plate.target_stir_rate = stir_plate.target_stir_rate * 2


# initialize all required elements
def home_system(stock_analyte, stock_titrant, max_reactor_volume, reactor_current_volume=0):
    new_titration.remaining_analyte_stock = stock_analyte
    new_titration.remaining_titrant_stock = stock_titrant
    new_titration.max_reactor_volume = max_reactor_volume
    new_titration.total_solution_in_flask = reactor_current_volume
    update_data()
    prime_syringe_pump()
    change_current_action_to_normal()


# prime the pump. Used in home system
def prime_syringe_pump():
    change_current_action("The system is priming the analyte pump.")
    syringe_pump_1.dispense_ml(volume_ml=3,
                               from_port=pump_stock_position,
                               to_port=pump_waste_position,
                               velocity_ml=pump_default_velocity,
                               wait=False
                               )
    change_current_action("The system is priming the titrant pump.")
    syringe_pump_2.dispense_ml(volume_ml=3,
                               from_port=pump_stock_position,
                               to_port=pump_waste_position,
                               velocity_ml=pump_default_velocity
                               )


# add analyte by a set volume
def add_analyte(volume):
    new_titration.total_volume_dispensed_analyte += volume
    new_titration.remaining_analyte_stock -= volume
    new_titration.total_solution_in_flask += volume
    update_data()
    change_current_action("The system is adding " + str(volume) + " milliliters of analyte.")
    syringe_pump_1.dispense_ml(volume_ml=volume,
                               from_port=pump_stock_position,
                               to_port=pump_reactor_position,
                               velocity_ml=pump_default_velocity
                               )
    change_current_action_to_normal()


# add titrant by a set volume
def add_titrant(volume):
    new_titration.total_volume_dispensed_titrant += volume
    new_titration.remaining_titrant_stock -= volume
    new_titration.total_solution_in_flask += volume
    update_data()
    change_current_action("The system is adding " + str(volume) + " milliliters of titrant.")
    syringe_pump_2.dispense_ml(volume_ml=volume,
                               from_port=pump_stock_position,
                               to_port=pump_reactor_position,
                               velocity_ml=pump_default_velocity
                               )
    change_current_action_to_normal()


# todo calibrate based on your reactor
# fill a 50 mL reactor
def fill_reactor(is_rinsing_cycle):
    if not is_rinsing_cycle:
        change_current_action("The system is filling the reactor.")
    peristaltic_pump_dispense.pump(pump_time=220,
                                   direction="dispense",
                                   rate=10
                                   )
    if not is_rinsing_cycle:
        change_current_action_to_normal()


# todo calibrate based on your reactor
# evacuate a 50 mL reactor
def evacuate_reactor(is_rinsing_cycle):
    if not is_rinsing_cycle:
        change_current_action("The system is evacuating the reactor.")
    peristaltic_pump_evacuate.pump(pump_time=250,
                                   direction="dispense",
                                   rate=10
                                   )
    new_titration.total_solution_in_flask = 0
    if not is_rinsing_cycle:
        change_current_action_to_normal()


# update titration data locally
def update_data():
    round_data()
    update_titration_status()
    update_volume_dispensed()
    overwrite_text_file(str(new_titration.max_reactor_volume), max_reactor_volume)
    overwrite_text_file(str(new_titration.remaining_titrant_stock), stock_titrant)
    overwrite_text_file(str(new_titration.remaining_analyte_stock), stock_analyte)
    overwrite_text_file(str(new_titration.total_solution_in_flask), current_reactor_volume)


# round data to two decimal places
def round_data():
    new_titration.total_volume_dispensed_analyte = round(new_titration.total_volume_dispensed_analyte, 2)
    new_titration.total_volume_dispensed_titrant = round(new_titration.total_volume_dispensed_titrant, 2)
    new_titration.total_solution_in_flask = round(new_titration.total_solution_in_flask, 2)
    new_titration.remaining_analyte_stock = round(new_titration.remaining_analyte_stock, 2)
    new_titration.remaining_titrant_stock = round(new_titration.remaining_titrant_stock, 2)
    new_titration.max_reactor_volume = round(new_titration.max_reactor_volume, 2)
    new_titration.exp_num = round(new_titration.exp_num, 2)


# report the status locally
def update_titration_status():
    overwrite_text_file(
        f"There are " + str(new_titration.remaining_titrant_stock) + " milliliters titrant stock solution"
        + " and " + str(new_titration.remaining_analyte_stock) + " milliliters analyte stock solution." "\n"
                                                                 "The total volume of solution in the flask is " + str(
            new_titration.total_solution_in_flask)
        + " milliliters." "\n" "The analyte solution dispensed is "
        + str(new_titration.total_volume_dispensed_analyte)
        + " milliliters, " + " and the titrant solution dispensed is "
        + str(new_titration.total_volume_dispensed_titrant) + " milliliters.", titration_status)


def update_volume_dispensed():
    overwrite_text_file(f"The analyte solution dispensed is " + str(new_titration.total_volume_dispensed_analyte)
                        + " milliliters, " + " and the titrant solution dispensed is "
                        + str(new_titration.total_volume_dispensed_titrant) + " milliliters.", final_report)


# Change this to False to exit loop
is_running = True


# exit the script. Activated by End Titration->Exit
def exit():
    rinsing_cycle()
    global is_running
    is_running = False


# wash the reactor
def rinsing_cycle():
    new_titration.total_volume_dispensed_analyte = 0
    new_titration.total_volume_dispensed_titrant = 0
    new_titration.total_solution_in_flask = 0
    update_data()
    change_current_action("The system is on its first rinse cycle.")
    evacuate_reactor(True)
    change_current_action("The system is on its second rinse cycle.")
    fill_reactor(True)
    evacuate_reactor(True)
    change_current_action("The system is on its third rinse cycle.")
    fill_reactor(True)
    evacuate_reactor(True)
    change_current_action_to_normal()


# change the speech to desired
def overwrite_text_file(str, file):
    f = open(file, "w")
    f.write(str)


def change_current_action(str):
    overwrite_text_file(str, current_action)


def change_current_action_to_normal():
    change_current_action("The system is waiting for commands.")


started = False


def main():
    overwrite_text_file("The system is inactive.", current_action)
    update_data()
    change_current_action_to_normal()
    while True and is_running:
        control_folder_contents = watch.contents
        if len(control_folder_contents) > 0:
            action_file_path = watch.oldest_instance()
            action_info = Path(action_file_path).read_text()
            json_acceptable_string = action_info.replace("'", "\"")
            json_acceptable_string = json_acceptable_string.replace("\n", "")
            json_acceptable_string = json_acceptable_string.replace("[", "{")
            json_acceptable_string = json_acceptable_string.replace("]", "}")
            action_info = json.loads(json_acceptable_string)
            action_uuid: str = list(action_info.keys())[0]
            executable: str = action_info[action_uuid]['executable']
            key_word_arguments: Dict = action_info[action_uuid]['key_word_arguments']
            try:
                print(f'execute: {executable} with key word arguments: {key_word_arguments}')
                exec(f'{executable}(**{key_word_arguments})')
                os.remove(action_file_path)
            except Exception as e:
                error_message = f'could not execute: {executable} with key word arguments: ' \
                                f'{key_word_arguments})\nError message:' + str(e)
                logger.warning(error_message)
        else:
            print('waiting for commands')
            time.sleep(5)


main()
